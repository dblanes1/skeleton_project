// import { StyleSheet, Text, View, AppRegistry, DeviceEventEmitter, TouchableOpacity, NativeModules , NativeEventEmitter } from 'react-native'
import { StyleSheet, Text, View, AppRegistry, TouchableOpacity, NativeModules, NativeEventEmitter } from 'react-native'

import React, {useEffect, useState} from 'react';
import  SunmiV2Printer  from "react-native-sunmi-v2-printer";

export default function App () {
  const [status, setStatus] = useState('');
  const [name, setName] = useState('Printing');
  const [result, setResult] = useState(''); 

 
  // useEffect(() => {
  //   console.log('App executed');
  // },[])
    useEffect(() => {
      console.log('App executed');
      const eventEmitter = new NativeEventEmitter(NativeModules.ToastExample);
    console.log(eventEmitter);
    let listener = null;
        try {
          //DeviceEventEmitter
         listener = eventEmitter.addListener('PrinterStatus', (action) => {
            switch(action) {
              case SunmiV2Printer.Constants.NORMAL_ACTION:
                setStatus(() => 'printer normal');
                break;
                case SunmiV2Printer.Constants.OUT_OF_PAPER_ACTION:
                  setStatus(() => 'printer out out page');
                  break;
                  case SunmiV2Printer.Constants.COVER_OPEN_ACTION:
                    setStatus(() => 'printer cover open');
                    break;
                    default: 
                    setStatus(() => 'printer status:' + action);
            }
          });
          }
           catch (error) {
            alert(error);
          console.log('error-> ', error);

          }
          return () => {
          if (listener) {
            listener.remove();
          } 
          } 
          console.log('status -> ', status);
      }, []);

    const print = async () => {

      try {
         // set aligment: 0-left,1-center,2-right
        await SunmiV2Printer.setAlignment(1);
        await SunmiV2Printer.setFontSize(30);
        await SunmiV2Printer.printOriginalText('NAME: DOUGLAS BLANES\n'); 
        await SunmiV2Printer.setFontSize(24);
        await SunmiV2Printer.setAlignment(1);
        await SunmiV2Printer.printOriginalText("===============================\n");
        await SunmiV2Printer.setAlignment(0);
        await SunmiV2Printer.printOriginalText('=== UI/UX DEVELOPER ===');
        await SunmiV2Printer.setAlignment(1);
        await SunmiV2Printer.printOriginalText("===============================\n");
        await SunmiV2Printer.printOriginalText('PROJECT: PRINTING\n');
        await SunmiV2Printer.printOriginalText('COMPANY: YSSP\n');
        await SunmiV2Printer.printOriginalText("\n\n\n\n");

      } catch (error) {
        alert(`error ${error}`);
        console.log(`error ${error}`);
      }
    }

    // const print2 = () => {
    //   console.log(`clicked ${name}`);
    //   alert(`clicked ${name}`);

    // }

  //render section
  return (
    <View style={styles.container}>
      <Text>Welcome to React Native</Text>
     <Text>Printer status: {status}</Text>
    <Text>sample name: {name}</Text> 
      <TouchableOpacity style={styles.button} onPress={() => print()}>
        <Text style={styles.buttonText}>Print</Text>
      </TouchableOpacity>
    </View>
  )
}

AppRegistry.registerComponent('skeleton_project', () => App);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 50,
    paddingHorizontal: 50,
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: 'steelblue',
  },
  buttonText: {
    color: 'white',
  },
});